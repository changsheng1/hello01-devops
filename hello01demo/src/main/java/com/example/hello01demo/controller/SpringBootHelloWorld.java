package com.example.hello01demo.controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
public class SpringBootHelloWorld {
    @GetMapping("/hello")
    public String hello() {
        return "hello Jenkins!";
    }
}